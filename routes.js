function getClients(req,response,client){
	const clientsSelectionQuery = {
		text: 'SELECT * FROM "clients"."client"'
	}
	client.query(clientsSelectionQuery, (err, res) => {
	if (err) {
		console.log(err.stack);
		response.send({
			success: false,
			code: 400,
			message: 'Erreur lors de la récupération de la liste des clients en base de donnée'
		});
		} else {
			const jsonObject={};
			const key = 'clients';
			const rows = res.rows;
			jsonObject[key] = [];
			for (var i = 0; i < rows.length; i++) { 
				var bddClient={
					"id":rows[i].id,
					"prenom":rows[i].prenom,
					"nom":rows[i].nom,
					"tel":rows[i].tel,
					"adresse":rows[i].adresse,
					"email":rows[i].email,
				};
				jsonObject[key].push(bddClient);
			}
			response.send({
				success: true,
				code: 200,
				data :jsonObject
			});
		}
	})
}

function getProduits(req,response,client){
	const produitsSelectionQuery = {
		text: 'SELECT * FROM "clients"."produit"'
	}
	client.query(produitsSelectionQuery, (err, res) => {
	if (err) {
		console.log(err.stack);
		response.send({
			success: false,
			code: 400,
			message: 'Erreur lors de la récupération de la liste des produits en base de donnée'
		});
		} else {
			const jsonObject={};
			const key = 'produits';
			const rows = res.rows;
			jsonObject[key] = [];
			for (var i = 0; i < rows.length; i++) { 
				var produit={
					"id":rows[i].id,
					"libelle":rows[i].libelle,
					"prix":rows[i].prix
				};
				jsonObject[key].push(produit);
			}
			response.send({
				success: true,
				code: 200,
				data :jsonObject
			});
		}
	})
}

function createClient(req,response,client){
	const UIDGenerator = require('uid-generator');
	const uidgen = new UIDGenerator();
	uidgen.generate((err, uid) => {
		if (err){
			console.log(err.stack);
			response.send({
				success: false,
				code: 400,
				message: "Error during generation of publication's uid"
			});
		} 
		else{
			const id=uid;
			const nom = req.body.nom;
			const prenom = req.body.prenom;
			const adresse = req.body.adresse;
			const tel = req.body.tel;
			const email = req.body.email;
			const clientInsertionQuery = {
				text: 'INSERT INTO "clients".client(id,nom,prenom,adresse,tel,email) values ($1,$2,$3,$4,$5,$6)',
				values: [id,nom,prenom,adresse,tel,email]
			}
			client.query(clientInsertionQuery, (err, res) => {
				if (err) {
					console.log(err.stack);
					response.send({
					success: false,
					code: 400,
					message: "Erreur durant la création du client en base de données"
				});
					} else {
						response.send({
							success: true,
							code: 200,
							message: 'Le client '+id+' a été inséré en base de données'
					});
				}
			});
		}
	})
};

function deleteClient(req,response,client){
	const id = req.params.id;
	const clientDeletionQuery = {
		text: 'DELETE from "clients"."client" where "clients"."client"."id" = $1',
		values: [id]
	}
	console.log("id : "+ id);
	client.query(clientDeletionQuery, (err, res) => {
		if (err) {
			console.log(err.stack);
			response.send({
				success: false,
				code: 400,
				message: "Erreur durant la suppression du client "+id
			});
		} else {
			response.send({
				success: true,
				code: 200,
				message: 'Le client '+id+' a été supprimé',
			});
		}
	})
};

function updateClientById(req,response,client){
	const id = req.params.id;
	const nom = req.body.nom;
	const prenom = req.body.prenom;
	const adresse = req.body.adresse;
	const tel = req.body.tel;
	const email = req.body.email;
	const clientUpdateQuery = {
		text: 'UPDATE "clients".client set nom=$1, prenom=$2, adresse=$3, email=$4, tel=$5 where id = $6',
		values: [nom,prenom,adresse,tel,email,id]
	};
	client.query(clientUpdateQuery, (err, res) => {
	if (err) {
		console.log(err.stack);
		response.send({
			success: false,
			code: 400,
			message: "Erreur durant la mise à jour du client "+id
		});
	} else {
		response.send({
			success: true,
			code: 200,
			message: 'Le client '+id+' a été mis à jour en base de donnée'
		});
	}
})};

function getClientById(req,response,client){
	console.log('GET CLIENT BY ID');
	const id = req.params.id;
	const clientSelectionQuery = {
		text: 'SELECT * FROM "clients"."client" where "clients"."client"."id" = $1',
		values: [id]
	}
	client.query(clientSelectionQuery, (err, res) => {
		if (err) {
			console.log(err.stack);
			response.send({
				success: false,
				code: 400,
				message: 'Erreur lors de la récupération du client '+id+' en base de données'
			});
		} else {
			const rows = res.rows;
			if(rows[0] == undefined){
				console.log("test3")
				response.send({
					success: false,
					code: 400,
					message : 'Le client '+id+' n`\'existe pas en base de données'
				});
			}else{
				const bddClient={
					"id":rows[0].id,
					"nom" :rows[0].nom,
					"prenom":rows[0].prenom,
					"adresse":rows[0].adresse,
					"tel":rows[0].tel,
					"email":rows[0].email,
				};
				const jsonObject={};
				const key="client";
				jsonObject[key] = [];
				jsonObject[key].push(bddClient);
				console.log("JSON OBJECT: "+jsonObject);
				response.send({
					success: true,
					code: 200,
					data : jsonObject
				});
			}
		}
	});
}

module.exports = {
	getClients,
	getProduits,
	createClient,
	deleteClient,
	updateClientById,
	getClientById
}


