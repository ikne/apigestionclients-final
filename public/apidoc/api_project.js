define({
  "name": "Gestion Clients API documentation",
  "version": "1.0.0",
  "description": "API pour la gestion des clients de l'application appgestionclients",
  "template": {
    "forceLanguage": "fr"
  },
  "sampleUrl": false,
  "defaultVersion": "0.0.0",
  "apidoc": "0.3.0",
  "generator": {
    "name": "apidoc",
    "time": "2020-03-29T20:11:22.628Z",
    "url": "http://apidocjs.com",
    "version": "0.17.7"
  }
});
