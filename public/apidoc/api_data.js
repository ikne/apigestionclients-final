define({ "api": [
  {
    "type": "delete",
    "url": "/clients/:id",
    "title": "Supprimer un client en fonction de son identifiant",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "id",
            "description": "<p>Identifiant du client</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message de succès</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n  \"message\": \"Le client XFQduC6vEDLTcunNkVLJWe a été supprimé\",\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.js",
    "groupTitle": "Clients",
    "name": "DeleteClientsId"
  },
  {
    "type": "get",
    "url": "/clients",
    "title": "Récupérer tous les clients",
    "group": "Clients",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "clients.id",
            "description": "<p>Identifiant du client</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "clients.prenom",
            "description": "<p>Prénom du client</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "clients.nom",
            "description": "<p>Nom du client</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "clients.tel",
            "description": "<p>Téléphone du client</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "clients.adresse",
            "description": "<p>Adresse du client</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "clients.email",
            "description": "<p>Adresse email du client</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n  \"id\": \"XFQduC6vEDLTcunNkVLJWe\",\n  \"prenom\": \"Enki\",\n  \"nom\": \"Michel\",\n  \"tel\": \"0651384631\",\n  \"adresse\": \"94 Chasse de la Madelenne 50110 Tourlaville\",\n  \"email\": \"ikne@hotmail.fr\",\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.js",
    "groupTitle": "Clients",
    "name": "GetClients"
  },
  {
    "type": "post",
    "url": "/clients",
    "title": "Ajouter un nouveau client",
    "group": "Clients",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clients.id",
            "description": "<p>Identifiant du client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clients.prenom",
            "description": "<p>Prénom du client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clients.nom",
            "description": "<p>Nom du client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clients.tel",
            "description": "<p>Téléphone du client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clients.adresse",
            "description": "<p>Adresse du client</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "clients.email",
            "description": "<p>Adresse email du client</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "message",
            "description": "<p>Message de succès</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": "HTTP/1.1 200 OK\n[{\n  \"message\": \"Le client XFQduC6vEDLTcunNkVLJWe a été ajouté en base de données\",\n}]",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Get Error",
          "content": "HTTP/1.1 500 Internal Server Error",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "./api.js",
    "groupTitle": "Clients",
    "name": "PostClients"
  }
] });
