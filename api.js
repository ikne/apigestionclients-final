//GESTION DES VARIABLES D ENVIRONNEMENT

require('dotenv').config({path: __dirname + '/.env'})

//DB CONNECTION

const { Pool, Client } = require('pg');
const client = new Client({
  user: process.env.DBUSER || process.env['db.user'],
  host: process.env.DBHOST || process.env['db.host'],
  database: process.env.DBNAME || process.env['db.database'],
  password: process.env.DBPASSWORD || process.env['db.password'],
  port: process.env.DBPORT || process.env['db.port'],
})
client.connect();

const hostname = process.env['app.hostname'] || '0.0.0.0';
const port =  process.env.PORT || process.env['app.port'];  

//API CONST

const express = require('express'); 
const app = express(); 
const bodyParser = require("body-parser");
const myRouter = express.Router(); 
const cors = require('cors');
const routes = require('./routes');

//ROUTING

app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));


myRouter.route('/')
//permet de prendre en charge toutes les méthodes. 
.all(function(req,res){ 
      res.json({message : "Bienvenu sur l'api de gestion des clients", methode : req.method});
});

// GET
/**
	 * @api {get} /clients Récupérer tous les clients
	 * @apiGroup Clients
	 * @apiSuccess {String} clients.id Identifiant du client
	 * @apiSuccess {String} clients.prenom Prénom du client
	 * @apiSuccess {String} clients.nom Nom du client
	 * @apiSuccess {String} clients.tel Téléphone du client
	 * @apiSuccess {String} clients.adresse Adresse du client
	 * @apiSuccess {String} clients.email Adresse email du client
	 * @apiSuccessExample {json} Success
	 *    HTTP/1.1 200 OK
	 *    [{
	 *      "id": "XFQduC6vEDLTcunNkVLJWe",
	 *      "prenom": "Enki",
	 *      "nom": "Michel",
	 *      "tel": "0651384631",
	 *      "adresse": "94 Chasse de la Madelenne 50110 Tourlaville",
	 *      "email": "ikne@hotmail.fr",

	 *    }]
	 * @apiErrorExample {json} Get Error
	 *    HTTP/1.1 500 Internal Server Error
	 */
myRouter.route('/clients')
.get(function(req,response){
	routes.getClients(req,response,client);
})
myRouter.route('/clients/:id')
.get(function(req,response){
	routes.getClientById(req,response,client);
})
myRouter.route('/produits')
.get(function(req,response){
	routes.getProduits(req,response,client);
})

//POST
/**
	 * @api {post} /clients Ajouter un nouveau client
	 * @apiGroup Clients
	 * @apiParam {String} clients.id Identifiant du client
	 * @apiParam {String} clients.prenom Prénom du client
	 * @apiParam {String} clients.nom Nom du client
	 * @apiParam {String} clients.tel Téléphone du client
	 * @apiParam {String} clients.adresse Adresse du client
	 * @apiParam {String} clients.email Adresse email du client
	 * @apiSuccess {String} message Message de succès
	 * @apiSuccessExample {json} Success
	 *    HTTP/1.1 200 OK
	 *    [{
	 *      "message": "Le client XFQduC6vEDLTcunNkVLJWe a été ajouté en base de données",
	 *    }]
	 * @apiErrorExample {json} Get Error
	 *    HTTP/1.1 500 Internal Server Error
	 */
myRouter.route('/clients')
.post(function(req,response){
	routes.createClient(req,response,client);
});
myRouter.route('/clients/:id')
.post(function(req,response){
	routes.updateClientById(req,response,client);
});

//DELETE
/**
	 * @api {delete} /clients/:id Supprimer un client en fonction de son identifiant
	 * @apiGroup Clients
	 * @apiParam {String} id Identifiant du client
	 * @apiSuccess {String} message Message de succès
	 * @apiSuccessExample {json} Success
	 *    HTTP/1.1 200 OK
	 *    [{
	 *      "message": "Le client XFQduC6vEDLTcunNkVLJWe a été supprimé",
	 *    }]
	 * @apiErrorExample {json} Get Error
	 *    HTTP/1.1 500 Internal Server Error
	 */
myRouter.route('/clients/:id')
.delete(function(req,response){
	routes.deleteClient(req,response,client);
});




//PREVENTING ATTACKS

 // Importing Dependencies
const rateLimit = require('express-rate-limit');
const helmet = require('helmet');
const mongoSanitize = require('express-mongo-sanitize');
const xss = require('xss-clean');

// Helmet
app.use(helmet());

// Rate Limiting
const limit = rateLimit({
    max: 100,// max requests
    windowMs: 60 * 60 * 1000, // 1 Hour of 'ban' / lockout 
    message: 'Too many requests' // message to send
});

// Body Parser
app.use(express.json({ limit: '10kb' })); // Body limit is 10

// Data Sanitization against NoSQL Injection Attacks
app.use(mongoSanitize());

// Data Sanitization against XSS attacks
app.use(xss());

 
// asking for the ap to use ratelimiting
app.use(myRouter,limit);  
 
// START API

app.listen(port, hostname, function(){
	console.log("Mon serveur fonctionne sur http://"+ hostname +":"+port);
	console.log("Mot de passe de la bdd : "+ client.database);
});


